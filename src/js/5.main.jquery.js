$('#owl-one').owlCarousel({
    loop:true,
    margin:16,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        992:{
            items:0
        }

    }
})
$('#owl-two').owlCarousel({
    loop:true,
    margin:16,
    nav:false,
    responsive:{
        0:{
            items:1
        }

    }
})
$('#owl-three').owlCarousel({
    loop:true,
    margin:16,
    nav:false,
    responsive:{
        0:{
            items:1
        },
        992:{
            items:2
        }

    }
})




$('.Dropdown').on('click','.Dropdown-title',function(){
	var t = $(this);
	var tp = t.next();
	var p = t.parent().siblings().find('p');
	tp.slideToggle();
	p.slideUp();
});

