    /* ---------- Scroll Animado ---------*/
    let scroll = new SmoothScroll('a[href*="#"]', {
        speed: 500,
        speedAsDuration: true
    });
    /* ---------- End Scroll Animado ---------*/




    /* ---------- Js Home ---------*/
    if (window.location.pathname === '/index.html' || window.location.pathname === '/') {


        /* ---------- Header ---------*/
        const headerContainer = document.getElementById("header")

        window.addEventListener("scroll", () => {
            if (window.scrollY > 0 && matchMedia('(min-width:62em)').matches) {
                headerContainer.classList.add("is-active")
            } else {
                headerContainer.classList.remove("is-active")
            }
        });

        /* ---------- Nav ---------*/
        const Toggler = document.getElementById('Toggler'),
            Container = document.getElementById('container')
        Toggler.addEventListener('click', () => {
            const Nav = document.getElementById('Nav'),
                close = document.getElementById('navClose')

            Nav.classList.toggle('is-active')
            Container.classList.toggle('is-blocked')

            close.addEventListener('click', () => {
                Nav.classList.remove('is-active')
                Container.classList.remove('is-blocked')
            })


            /* ---------- cerrar menu al dar click en Links ---------*/

            if (matchMedia('(max-width:62em)'.matches)) {
                const links = Array.from(document.querySelectorAll('.Nav-link'))
                links.map(e => {
                    e.addEventListener('click', () => {
                        Nav.classList.remove('is-active')
                        Container.classList.remove('is-blocked')
                    })
                })
            }


        })




        /* ---------- End Nav ---------*/


        /* ---------- Active Modal ---------*/

        const btnsContainer = document.getElementById('Buttons')
        const modalContainer = document.getElementById('modal')

        btnsContainer.addEventListener('click', e => {
            e.stopPropagation()
            Container.classList.toggle('is-blocked')
            if (e.target.classList.contains('Buttons-link')) {
                const target = e.target.parentElement.parentElement.parentElement
                target.querySelector('.Modal').classList.toggle('is-show')
            } else if (e.target.classList.contains('targetBtn')) {
                const targetBtn = e.target.parentElement.parentElement.parentElement.parentElement
                targetBtn.querySelector('.Modal').classList.toggle('is-show')
            }
        })

        modalContainer.addEventListener('click', e => {
            e.stopPropagation()
            Container.classList.toggle('is-blocked')
            if (e.target.classList.contains('Modal')) {
                e.target.classList.toggle('is-show')
            } else if (e.target.classList.contains('close')) {
                const target = e.target.parentElement.parentElement.parentElement
                target.classList.toggle('is-show')
            }
        })


        /* ---------- End Active Modal ---------*/

        /* ---------- Active Modal Terminos y Condiciones ---------*/

        const modalContainerTerminos = document.getElementById('modalTerminos'),
            btnTerminosD = document.getElementById('btnTerminosD'),
            btnTerminosM = document.getElementById('btnTerminosM')


        btnTerminosD.addEventListener('click',activeModalTerminos)
        btnTerminosM.addEventListener('click',activeModalTerminos)

        Container.addEventListener('click', e =>{
            if(!e.target.classList.contains('ModalTerminos') && !e.target.classList.contains('ModalTerminos-close')){

            }else{
                modalContainerTerminos.classList.remove('is-show')
            }
        })

        function activeModalTerminos(){
            modalContainerTerminos.classList.add('is-show')
        }

        /* ---------- End Active Modal Terminos y Condiciones ---------*/

    }

    /* ---------- End Js Home ---------*/

    /* ---------- Nav ---------*/
    const Toggler = document.getElementById('Toggler')

    Toggler.addEventListener('click', () => {
        const Nav2 = document.getElementById('Nav2')
        Nav2.classList.toggle('is-show')
    })

    /* ---------- Js Registro ---------*/
    if (window.location.pathname === '/registro.html') {


        /* ---------- Modal Map ---------*/

        const iconMap = document.getElementById('iconMap'),
            modalContainer = document.getElementById('ModalMap'),
            iconMapMobile = document.getElementById('iconMapMobile')
        if (iconMap) {
            iconMap.addEventListener('click', () => {
                modalContainer.classList.add('is-show')


                modalContainer.addEventListener('click', e => {
                    e.stopPropagation()
                    if (e.target.classList.contains('ModalMap')) {
                        e.target.classList.remove('is-show')
                    }
                })
            })

        }
        if (iconMapMobile) {
            iconMapMobile.addEventListener('click', () => {
                modalContainer.classList.add('is-show')


                modalContainer.addEventListener('click', e => {
                    e.stopPropagation()
                    if (e.target.classList.contains('ModalMap')) {
                        e.target.classList.remove('is-show')
                    }
                })
            })

        }




        /* ---------- End Modal Map ---------*/


    }

    /* ---------- Pedidos.html ---------*/
    if (window.location.pathname === '/pedidos.html') {
        /* ---------- Variables ---------*/
        const ModalContainer = document.getElementById('ModalContainer')
        const PedidosContainer = document.getElementById('PedidosContainer')
        const ModalEditContainer = document.getElementById('ModalEdit')

        /* ---------- Listeners ---------*/

        cargarListeners()

        function cargarListeners() {
            ModalContainer.addEventListener('click', ModalPedidosShow)
            PedidosContainer.addEventListener('click', showMoreMenu)
            ModalEdit.addEventListener('click', showModalEdit)
        }

        /* ---------- Funciones ---------*/

        function ModalPedidosShow(e) {
            if (e.target.classList.contains('ModalPedido')) {
                e.target.classList.toggle('is-hidden')
            } else if (e.target.classList.contains('ModalPedido-close')) {
                e.target.parentElement.parentElement.classList.toggle('is-hidden')
            }
        }

        function showMoreMenu(e) {
            const MenusActive = document.querySelectorAll('.MenuRightClick')
            const ModalEdit = document.querySelector('.ModalEdit')

            if (e.target.classList.contains('BtnMore')) {
                MenusActive.forEach(e => e.classList.remove('is-active'))
                e.target.firstChild.classList.toggle('is-active')
            } else if (e.target.classList.contains('BtnEdit')) {
                ModalEdit.classList.remove('is-hidden')
            } else {
                MenusActive.forEach(e => e.classList.remove('is-active'))
            }
        }

        function showModalEdit(e) {
            if (e.target.classList.contains('ModalEdit')) {
                e.target.classList.toggle('is-hidden')
            } else if (e.target.classList.contains('ModalEdit-close')) {
                e.target.parentElement.parentElement.classList.toggle('is-hidden')
            }

        }
    }